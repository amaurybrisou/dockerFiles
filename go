FROM ubuntu:14.04
MAINTAINER Amaury Brisou

#ENTRYPOINT echo 'This is a GO Docker Image'

#install git
RUN apt-get update
RUN apt-get install -y git curl

# install go
RUN curl -s https://go.googlecode.com/files/go1.2.linux-amd64.tar.gz | tar -v -C /usr/local/ -xz

# setup go env
ENV PATH  /usr/local/go/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin
ENV GOPATH  /root/go
ENV GOROOT  /usr/local/go


# ADD my sources to /root/go
ADD . /root/go/
# set working dir
WORKDIR /root/go
 	
# expose port 8080
EXPOSE 8080
